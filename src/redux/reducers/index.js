import { combineReducers } from 'redux';

import AuthReducer from './../../screen/login/AuthReducer';
import LandingReducer from './../../screen/landing/LandingReducer';

export default combineReducers({
    landing: LandingReducer,
    auth: AuthReducer,
    // homeAuth: HomeReducer,
    // equipmentAuth: EquipmentReducer,

});