import React, { Component } from 'react';
import { Alert, StatusBar, ProgressBarAndroid, Text, View, TouchableOpacity, TouchableHighlight, Image, TextInput, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import colors from '../../style/colors';
import Spinner from 'react-native-loading-spinner-overlay';
import PasswordInputText from 'react-native-hide-show-password-input';
import { TextField } from 'react-native-material-textfield';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import { usernameChanged, passwordChanged, emailChanged, loginUser, forgotPasswordUser } from './AuthAction';
import GeneralStatusBarColor from './../../components/GeneralStatusBarColor';

class LoginScreen extends Component {
    state = {
        password: '',
        visibleModal: null,
    };

    onUsernameChange(text) {
        this.props.usernameChanged(text)
    }

    onPasswordChange(text) {
        this.props.passwordChanged(text);
    }

    onEmailChange(text) {
        this.props.emailChanged(text);
    }
    isDisabled() {
        this.props.username === '' || this.props.password === ''
    }

    login() {
        const { username, password } = this.props;
        this.props.loginUser = ({ username, password });

    }
    back() {
        this.props.navigation.navigate('Landing')
    }
    // send(onPress) {
    //     this.setState({ visibleModal: 2 })
    //     const { email } = this.props;
    //     this.props.forgotPasswordUser({ email });

    // }

    _renderButton = (text, onPress) => (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.forgotButton}>
                <Text style={styles.forgotButton}>{text}</Text>
            </View>
        </TouchableOpacity>
    );

    _renderClosed = (text, onPress) => (

        <TouchableOpacity onPress={onPress}>
            <View style={styles.forgotButton}>
                <Text style={styles.forgotButton}>{text}</Text>
            </View>
        </TouchableOpacity>

    );

    // _renderModalContent = () => (
    //     <View style={styles.modalContent}>
    //         {this._renderClosed('Close', () => this.setState({ visibleModal: null }))}
    //         <Text style={styles.logoText}>Forgot your password?</Text>
    //         <Text>Enter your email below to recice your password reset instruction</Text>
    //         <TextInput
    //             style={styles.inputBox}
    //             placeholder="Input email address"
    //             placeholderTextColor="#a9a9a9"
    //             selectionColor="#fff"
    //             keyboardType="email-address"
    //             onChangeText={(this.onEmailChange.bind(this))}
    //             value={this.props.email} />
    //         {this._renderButtonSend('Send', () => this.setState({ visibleModal: 2 }))}
    //     </View>
    // );

    // _renderButtonSend = (text, onPress) => {
    //     return (
    //         <View style={styles.modalClose} >
    //             <TouchableOpacity
    //                 disabled={!(this.props.email)}
    //                 onPress={() => this.send(onPress)}>
    //                 <View style={styles.forgotButton}>
    //                     <Text style={styles.forgotButton}>{text}</Text>
    //                 </View>
    //             </TouchableOpacity>
    //         </View>

    //     )
    // }

    render() {
        return (
            <View style={styles.containerTop}>
                <StatusBar backgroundColor="#FFF" barStyle="light-content" />
                <Spinner
                    visible={this.props.loading}
                    textContent={'Loading...'}
                    textStyle={styles.spinnerTextStyle} />
                {/* <Modal
                    style={styles.successModal}
                    isVisible={this.state.visibleModal === 2}
                    animationIn={'slideInLeft'}
                    animationOut={'slideOutRight'}>
                    
                </Modal>
                <Modal isVisible={this.state.visibleModal === 5} style={styles.bottomModal}>
                    {this._renderModalContent()}
                </Modal> */}

                <View style={styles.container}>
                    <TouchableOpacity onPress={this.back.bind(this)}>
                        <Image style={styles.imageBack} source={require('../../assets/icons/buttonBack.png')} />
                    </TouchableOpacity>

                    <Text style={styles.logoText}>Log In</Text>

                    <TextField
                        //tintColor='#bfbfbf'
                        //textColor='#bfbfbf'
                        baseColor='#bfbfbf'
                        label='Input Username'
                        value={this.props.username}
                        onChangeText={(this.onUsernameChange.bind(this))} />

                    <PasswordInputText
                        // tintColor='#bfbfbf'
                        //textColor='#bfbfbf'
                        baseColor='#bfbfbf'
                        value={this.props.password}
                        onChangeText={this.onPasswordChange.bind(this)} />

                    <View style={styles.containerButton}>
                        <TouchableHighlight
                            style={!this.props.username || !this.props.password ? styles.buttonDisable : styles.button}
                            disabled={!(this.props.username && this.props.password)}
                            // style={styles.button}
                            onPress={this.login.bind(this)}>
                            <Text style={!this.props.username || !this.props.password ? styles.buttonTextDisable : styles.buttonText}>Log In</Text>
                        </TouchableHighlight>

                        <View style={styles.signupTextCont}>
                            {this._renderButton('Forgot Password ?', () => this.setState({ visibleModal: 5 }))}
                            {/* <TouchableOpacity onPress={this.forgot.bind(this)}><Text style={styles.signupButton}> Forgot Password ?</Text></TouchableOpacity> */}
                        </View>
                    </View>

                </View>
            </View>
        )
    }
}

const mapStateToProps = ({ auth }) => {
    const { username, password, email, error, loading, success } = auth;
    return { username, password, email, error, loading, success };
};

export default connect(mapStateToProps, { usernameChanged, passwordChanged, emailChanged, loginUser, forgotPasswordUser })(LoginScreen);
// export default LoginScreen;



const styles = StyleSheet.create({
    containerTop: {
        marginTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
        flexGrow: 1,
        justifyContent: 'flex-start',
    },
    containerButton: {
        //justifyContent:'center',
        //alignItems: 'center'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    inputBox: {
        height: 40,
        backgroundColor: 'rgba(255,255,255, 0.8)',
        paddingLeft: 10,
        marginBottom: 15,
        borderRadius: 3,
    },
    errorText: {
        fontSize: 15,
        fontFamily: 'SFUIDisplay-Thin',
        color: 'red',
    },
    modalContent: {
        backgroundColor: 'white',
        padding: 22,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalSuccess: {
        backgroundColor: 'white',
        padding: 22,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        paddingBottom: 300,
    },
    modalClose: {
        backgroundColor: 'white',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
        paddingBottom: 300,
    },
    bottomModal: {
        justifyContent: 'flex-end',
        //padding:80,
        width: wp('100%'),
        height: hp('90%'),
        margin: 0,

    },
    successModal: {
        width: wp('100%'),
        height: hp('90%'),
        margin: 0,
    },
    container: {
        flex: 1,
        padding: 10,
        paddingTop: 30,
    },

    imageBack: {
        width: wp('7%'),
        height: hp('4%'),
        resizeMode: 'stretch'
    },

    button: {
        width: wp('40%'),
        height: hp('6%'),
        backgroundColor: colors.yellow,
        borderRadius: 4,
        marginVertical: 10,
        paddingVertical: 13,
        marginTop: 20,
    },
    buttonDisable: {
        width: wp('40%'),
        height: hp('6%'),
        backgroundColor: colors.yellowDisable,
        borderRadius: 4,
        marginVertical: 10,
        paddingVertical: 13,
        marginTop: 20,
    },

    buttonTextDisable: {
        fontSize: 12,
        fontWeight: '500',
        color: '#737373',
        textAlign: 'center',
        fontFamily: 'SFUIDisplay-Medium',
    },
    buttonText: {
        fontSize: 12,
        fontWeight: '500',
        color: colors.black,
        textAlign: 'center',
        fontFamily: 'SFUIDisplay-Medium',
    },
    signupTextCont: {
        paddingVertical: 16,
        flexDirection: 'row'
    },

    signupText: {
        color: '#212121',
        fontSize: 16
    },

    forgotButton: {
        color: colors.yellowText,
        fontSize: 12,
        fontWeight: '500',
        fontFamily: 'SFUIDisplay-Thin'
    },


    logoText: {
        marginTop: 20,
        color: '#212121',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'left',
        paddingVertical: 2,
        fontFamily: 'SFUIDisplay-Medium',
    },
    logoSuccess: {
        marginTop: 20,
        color: '#212121',
        fontSize: 14,
        textAlign: 'center',
        paddingVertical: 2,
        fontFamily: 'SFUIDisplay-Thin'
    },

    logoLog: {
        color: '#212121',
        fontSize: 24,
        textAlign: 'left',
        fontWeight: 'bold',
    },
});

