/**
 * @format
 */

import React, { Component } from 'react';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Splash from './src/screen/splashscreen/Splash';
import AppNavigator from './src/navigation/AppNavigator';
import Landing from './src/screen/landing/Landing';
import { YellowBox } from 'react-native';

YellowBox.ignoreWarnings([
  'Warning: componentWillMount is deprecated',
  'Warning: componentWillReceiveProps is deprecated',
  'Warning: componentWillUpdate is deprecated',
  'Warning: Encountered two children with the same key',
  'Warning: Cannot update during an existing state transition (such as within `render`)',
  'Module RCTImageLoader requires',
]);

class Main extends Component {
    constructor(props){
        super(props);
        this.state = { currentScreen: 'Splash'};
        setTimeout(()=>{
            this.setState({ currentScreen: 'Landing'})
        }, 500)
    }
    render(){
        const { currentScreen } = this.state
        let mainScreen = currentScreen === 'Splash' ? <Splash /> : <AppNavigator />
        return mainScreen
    }
}

AppRegistry.registerComponent(appName, () => Main);
