/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import AppNavigator from './navigation/AppNavigator';
import { YellowBox } from 'react-native';

YellowBox.ignoreWarnings([
  'Warning: componentWillMount is deprecated',
  'Warning: componentWillReceiveProps is deprecated',
  'Warning: componentWillUpdate is deprecated',
  'Warning: Encountered two children with the same key',
  'Warning: Cannot update during an existing state transition (such as within `render`)',
  'Module RCTImageLoader requires',
]);

export default class App extends React.Component {
  render() {
    return (
      <AppNavigator />  
    );
  }
}

