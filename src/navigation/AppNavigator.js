import React, { Component } from 'react';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import LandingScreen from '../screen/landing/Landing';
import Splash from '../screen/splashscreen/Splash'
import LoginScreen from '../screen/login/Login.screen';
import reducers from '../redux/reducers';
import ReduxThunk from 'redux-thunk';
import {
    createStore,
    applyMiddleware,
    combineReducers,
} from 'redux';
import { Provider, connect } from 'react-redux';

const RootNavigator = createStackNavigator({
    Landing: {
        screen: LandingScreen,
        navigationOptions: {
            header: null
        }
    },
    SplashScreen: {
        screen: Splash,
        navigationOptions: {
            header: null
        }
    },
    Login: {
        screen: LoginScreen,
        navigationOptions: {
            header: null
        }
    },
    // Main: {
    //     screen: MainTabBar,
    //     navigationOptions: {
    //         header: null,
    //         gesturesEnabled: false,
    //     },
    // }
});


const AppNavigator = createAppContainer(RootNavigator)

const store = createStore(reducers,{}, applyMiddleware(ReduxThunk));

class Root extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <AppNavigator />
            </Provider>
        );
    }
}
export default Root
