import { GET_USERNAME,
    ERROR_STORAGE  } from './../../redux/types/type';

const INITIAL_STATE = { 
   msg: '',
   error: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_USERNAME:
            return { ...state, error: "SUCCESS"};
        case ERROR_STORAGE:
            return { ...state, error: ''};
        default:
            return state;
    }
};