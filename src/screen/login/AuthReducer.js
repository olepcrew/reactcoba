import { USERNAME_CHANGED, 
    PASSWORD_CHANGED, 
    EMAIL_CHANGED,
    LOGIN_USER_SUCCESS, 
    LOGIN_USER_FAIL, 
    LOGIN_USER, 
    LOGIN_USER_REQUIRED,
    FORGOT_FAILED,
    FORGOT_REQ,
    FORGOT_SUCCESS } from './../../redux/types/type';

const INITIAL_STATE = { 
    username: '',
    password: '',
    email: '',
    error: '',
    success: [],
    forgotSuccess: [],
    loading: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case USERNAME_CHANGED:
            return { ...state, username: action.payload };
        case PASSWORD_CHANGED:
            return { ...state, password: action.payload };
        case EMAIL_CHANGED:
            return { ...state, email: action.payload };
        case LOGIN_USER_SUCCESS:
            return { ...state, INITIAL_STATE,  loading: false, success: action.payload, error: 'SUCCESS' };
        case LOGIN_USER_FAIL:
            return { ...state, error: action.payload, loading: false };
        case LOGIN_USER:
            return { ...state, loading: true,  error: ''  };
        case FORGOT_REQ:
            return { ...state, error: ''  };
        case FORGOT_FAILED:
            return { ...state, error: action.payload };
        case FORGOT_SUCCESS:
            return { ...state, INITIAL_STATE, forgotSuccess: action.payload, };
        case LOGIN_USER_REQUIRED:
            return { ...state, error: 'Please check your username or password!'  };
        default:
            return state;
    }
};