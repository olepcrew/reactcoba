export default {
    white: '#FFFFFF',
    black: '#000000',
    yellow: '#FFD500',
    yellow800: '#F9A825',
    yellowDisable: '#ffeb99',
    yellowText: '#C2A100',
    gray: '#AAAAAA',
    green: '#76D672'
};