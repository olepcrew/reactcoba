import {
    GET_USERNAME,
    ERROR_STORAGE
} from './../../redux/types/type';
import AsyncStorage from '@react-native-community/async-storage';


export const getUsername = () => {
    return (dispatch) => {

        AsyncStorage.getItem('data', (error, result) => {
            if (result) {
                let resultParsed = JSON.parse(result)
                // alert(JSON.stringify(resultParsed))
                if (resultParsed.userName != null) {

                    dispatch({ type: GET_USERNAME, payload: resultParsed.username })
                }
            }
            if (error) {
                dispatch({ type: ERROR_STORAGE, payload: error })
            }
        });

    };
};