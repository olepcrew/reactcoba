import React, { Component } from 'react';
import { View, Image, StyleSheet, StatusBar } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as Progress from 'react-native-progress';

export default class Splash extends Component {
    render() {
        return (
            <View style={styles.wrapper}>
                <StatusBar backgroundColor="#ffd500" barStyle="light-content" />
                <Image style={styles.logo} source={require('../../assets/images/logoUnitedTractor.png')} />
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'stretch' }}>
                    <View style={{ width: '40%', height: 5, backgroundColor: 'green', borderTopLeftRadius: 6, borderBottomLeftRadius: 6 }} />
                    <View style={{ width: '40%', height: 5, backgroundColor: 'red', borderTopRightRadius: 6, borderBottomRightRadius: 6 }} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: '#ffd500',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    icon: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    logo: {
        marginTop: 350,
        width: wp('55%'),
        height: hp('6%'),
        resizeMode: 'stretch'
    },
    title: {
        justifyContent: 'center',
        color: 'white',
        fontSize: 35,
        fontWeight: 'bold'
    }
});
