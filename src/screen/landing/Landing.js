import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import colors from '../../style/colors';
import * as Progress from 'react-native-progress';
import {Card, Button} from 'native-base';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
// import ListView from ' deprecated-react-native-listview';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  FlatList,
  ProgressBarAndroid,
  TouchableOpacity,
} from 'react-native';
import Category from '../../components/Category'
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { ScrollView } from 'react-native-gesture-handler';
import axios from 'axios';
import {ListItem } from 'react-native-elements'

const data = [
  { name: 'item1',
index:0 },
  { name: 'item2',
index:1 },
  { name: 'item3',
index:2 },
  { name: 'item4',
index:3 },
  { name: 'item5',
index:4 },
  { name: 'item6',
index:5 },
  { name: 'item7',
index:6 },
  { name: 'item8',
index:7 },
  { name: 'item9',
index:8 }
];

class Landing extends Component {

  constructor(props) {
    super(props)
    // prefik_url = 'http://wadaya.rey1024.com/upload/kategori/';
    this.state = {
      select : false,
      index:'',
      type:'',
      articles: []

    };
    
  }



  componentDidMount() {
    // axios.get('http://mhs.rey1024.com/apibudaya/getListCategory.php')
  // axios.get('https://api.gigel.co.id/api/v1/test/react-native')
  //   .then(res => {
  //     alert(JSON.stringify(res))
  //     const articles = res.data;
  //     console.log(articles);
  //     this.setState({ articles });
  //   })
      // .catch(err => {
      //   alert(JSON.stringify(err))
      // }
      // )


      axios.get('https://api.gigel.co.id/api/v1/test/react-native')
      .then(res => {
        // alert(JSON.stringify(res))
        for(let i = 1; i < 10; i++){
            let data = {
              // content: content['data'][i]['content'],
              description: res['data'][i]['description'],
              author: res['data'][i]['author'],
              url: res['data'][i]['url'],
              title: res['data'][i]['title'],
              publishedAt: res['data'][i]['publishedAt'],
              urlToImage: res['data'][i]['urlToImage'],
            }
        }
          AsyncStorage.setItem('data', JSON.stringify(data))
          alert(JSON.stringify(res))
      })
  }

  keyExtractor = (item, index) => index.toString()
  renderItem = ({ item }) => (
  <ListItem
    title={item.data}
    
    // leftAvatar={{ source: { uri: item.thumbnailUrl } }}
  />
)
  
  componentWillMount() {
    // this.props.getUsername()
  }

  handleLoginButton() {
    this.props.navigation.navigate('Login')
  }
  handleRegisterButton() {
    this.props.navigation.navigate('SplashScreen')

  }

  _renderTelementary() {
    return (
        <View style={styles.container}>

            <ProgressBarAndroid
                styleAttr="Horizontal"
                indeterminate={false}
                progress={0.2270}
            />
        </View>
    )
  }

  renderFooter = () => {
    if (this.state.refreshing) {
      return <ActivityIndicator size="large" />;
    } else {
      return null;
    }
  };

  actionClick(item){
    this.state.type = item
    this.state.select = true
    alert(item+"====="+this.state.type)
  }

  fetchMore = () => {
    if (this.state.refreshing){
      return null;
    }
    this.setState(
      (prevState) => {
        return { refreshing: true, pageNum: prevState.pageNum + 1 };
      },
      () => {
        this.sendAPIRequest(null , true);
      }
    );
  };


  
  
  render() {

    return (

      <View style={styles.wrapper}>
        <StatusBar backgroundColor="#92b8f6" barStyle="light-content" />
        
        {/* <ImageBackground style={styles.backgroundLogo} source={require('./src/assets/images/login-photo.png')} /> */}

        {/* <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}> */}
        <ImageBackground style={styles.backgroundLogo} source={require('../../assets/images/UT2.png')}>
          <Image style={styles.logo} source={require('../../assets/images/logoUnitedTractor.png')} />
          <Progress.Bar progress={0.5} width={200} marginLeft={125} color={'green'} animated={true} animationType={"spring"} indeterminateAnimationDuration={500} />
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 10, paddingRight: 10, paddingTop: 10, alignItems: 'flex-start' }}>
                <Card
                  style={styles.cardViewStyle}>
                    <Text style={styles.textButton}> Unit ABCDEFG </Text>
                </Card>
                <Card
                  style={styles.cardViewStyle}>
                    <Text style={styles.textButton}> Stok GHIJKLMN </Text>
                </Card>
                <Card
                  style={styles.cardViewStyle}>
                    <Text style={styles.textButton}> Barang ABCDE </Text>
                </Card>
                <Card
                  style={styles.cardViewStyle}>
                    <Text style={styles.textButton}> Jumlah GHIJKLMN </Text>
                </Card>
                <Card
                  style={styles.cardViewStyle}>
                    <Text style={styles.textButton}> Antrian AAAAAAA </Text>
                </Card>
                <Card
                  style={styles.cardViewStyle}>
                    <Text style={styles.textButton}> Datang BBBBBBB </Text>
                </Card>
                <Card
                  style={styles.cardViewStyle}>
                    <Text style={styles.textButton}> Keluar CCCCCC </Text>
                </Card>
                <Card
                  style={styles.cardViewStyle}>
                    <Text style={styles.textButton}> Sisa DDDDDDD </Text>
                </Card>
            </View>
            </ScrollView>

            <ScrollView horizontal={true}>
                        <View style={{ flex: 0 }}>
                            <FlatList
                                data={data}
                                renderItem={({ item }) => (
                                    <View style={{ flex:0, paddingRight: 5, paddingTop: 10, paddingLeft:10}}>
                                        <TouchableOpacity onPress={() => this.actionClick(item.name)} >
                                            <Text style={item.name != this.state.type ? styles.menuNonActive: styles.menuActive }>
                                              {item.name}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                )}
                                numColumns={30}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    </ScrollView>


          <View style={styles.container} >
            <View style={styles.header}>
              <Text style={styles.txtHeader}> API => </Text>
            </View>

            <FlatList
               keyExtractor={this.keyExtractor}
               data={this.state.articles}
               renderItem={this.renderItem}
               onEndReached={this.fetchMore}
               onEndReachedThreshold={0.1}
               ListFooterComponent={this.renderFooter}
               refreshing={this.state.refreshing}
             />
           </View>
          
              {/* <View style={{flex:0, height:60, width:450,backgroundColor:'white', paddingTop:20, borderWidth:0.5, borderColor:'#dddddd'}}>
                
                  <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                    <Category />
                    <Category />
                    <Category />
                    <Category />
                  </ScrollView>
                
              </View> */}


          
          
          
          <View style={styles.container}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.logInButton}
                onPress={() => { this.handleLoginButton() }}>
                <Text style={[styles.textButton]}>Login</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.registerButton}
                onPress={() => { this.handleRegisterButton() }}>
                <Text style={[styles.textButtonRegister]}>Register</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.registerButton}
                onPress={() => { this.handleRegisterButton() }}>
                <Text style={[styles.textButtonRegister]}>Register</Text>
              </TouchableOpacity>
            </View>
            </ScrollView>
          </View>
        </ImageBackground>
      </View>

    );
  }
}

const mapStateToProps = ({ Landing }) => {
  // const { error } = Landing;
  return { };
};

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
       flex: 1,
  },
  menuNonActive:{
    color:'#3b3934',
    
  },
  menuActive: {
    color: '#ffd500',
  },
  txtHeader: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color:'#fff'
  },
  header: {
    height:70,
    backgroundColor:'brown',
    justifyContent:'center',
    alignItems:'center'
  },
  containerDash: {
    flex: 0,
    width: wp('29%'),
    height: hp('15%'),
    borderRadius: 10,
    padding: 10,
},
  cardViewStyle:{
    marginTop:20,
    width: 150, 
    height: 25,
 
  },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },

  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    marginLeft: 10,
    flex: 0.5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },

  backgroundLogo: {
    width: wp('100%'),
    height: hp('100%'),
    resizeMode: 'cover',
    flex: 1,
  },
  logo: {
    marginTop: 80,
    marginLeft: 100,
    width: wp('60%'),
    height: hp('16%'),
    resizeMode: 'center',
  },
  buttonContainer: {
    flex: 1,
  },
  registerButton: {
    width: wp('40%'),
    height: hp('6%'),
    marginRight: 40,
    marginLeft: 14,
    padding: 13,
    backgroundColor: colors.yellow,
    borderRadius: 10,

  },
  logInButton: {
    width: wp('40%'),
    height: hp('6%'),
    alignItems: 'center',
    marginRight: 14,
    marginLeft: 10,
    padding: 13,
    backgroundColor: colors.white,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: '#254484',

  },
  textButton: {
    color: '#2a2a2a',
    fontSize: 14,
    textAlign: 'center',
    fontFamily: 'SFUIDisplay-Medium',
  },
  textButtonRegister: {
    color: '#2a2a2a',
    fontSize: 12,
    textAlign: 'center',
    alignItems: 'center',
    fontFamily: 'SFUIDisplay-Medium',
  },
});

export default connect(mapStateToProps)(Landing);
// export default Landing;